package cn.myzf.netty.client.initializer;

import cn.myzf.netty.client.heart.NettyClient;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @package cn.myzf.netty.client.initializer
 * @Date Created in 2019/12/6 14:54
 * @Author myzf
 */
public class ClientStart implements ApplicationContextInitializer {




    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        start();
    }

    private void start() {
        try {
            NettyClient client = new NettyClient();
            client.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
